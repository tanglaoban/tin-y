# node ./ImageCompression.js
# exec /bin/bash
#!/bin/sh  

#============ get the file name ===========  

# echo -e "请输入你要读取的文件夹路径\n当前路径为${PWD}"  

echo -e "拖拽想要压缩的文件和文件夹到此"  

read InputDir  

echo "开始压缩${InputDir}"  

./bin/node ./ImageCompression.js "${InputDir}"

exec /bin/bash